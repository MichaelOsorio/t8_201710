package model.VO;

public class VOArco<K> implements Comparable<VOArco>{

	K idDestino;
	double peso;
	char ordenLexicografico;
	
	public void setPeso( double peso){
		this.peso = peso;
	}
	
	public void setOrdenLexicografico(char orden){
		this.ordenLexicografico = orden;

	}
	
	public void setId(K id){
		this.idDestino = id;
	}

	public K getId(){
		return this.idDestino;
	}
	
	public double getPeso(){
		return this.peso;
	}
	
	public char getOrdenLexicografico()
	{
		return this.ordenLexicografico;
	}

	@Override
	public int compareTo(VOArco o) {
		
		int retorno =0;
		 if(ordenLexicografico < o.getOrdenLexicografico()){retorno = -1;}
		 else if(ordenLexicografico > o.getOrdenLexicografico()){retorno = 1;}
		 
		return retorno;	
	}
}
