package model.data.structures;

import java.util.Stack;

/**
 *  The {@code BreadthDirectedFirstPaths} class represents a data type for finding
 *  shortest paths (number of edges) from a source vertex <em>s</em>
 *  (or set of source vertices) to every other vertex in the digraph.
 *  <p>
 *  This implementation uses breadth-first search.
 *  The constructor takes time proportional to <em>V</em> + <em>E</em>,
 *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
 *  It uses extra space (not including the digraph) proportional to <em>V</em>.
 *  <p>
 *  For additional documentation, 
 *  see <a href="http://algs4.cs.princeton.edu/42digraph">Section 4.2</a> of 
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class BreadthFirstDirectedPaths<K extends Comparable<K>, V> {
    private static final int INFINITY = Integer.MAX_VALUE;
    private boolean[] marked;  // marked[v] = is there an s->v path?
    private int[] edgeTo;      // edgeTo[v] = last edge on shortest s->v path
    private int[] distTo;      // distTo[v] = length of shortest s->v path

    /**
     * Computes the shortest path from {@code s} and every other vertex in graph {@code G}.
     * @param G the digraph
     * @param s the source vertex
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public BreadthFirstDirectedPaths(WeightedDirectedGraph<K, V> G, K s, ListaEncadenada<NodoCamino<K>> nodoCamino) {
        marked = new boolean[G.numVertices()];
        distTo = new int[G.numVertices()];
        edgeTo = new int[G.numVertices()];
        for (int v = 0; v < G.numVertices(); v++)
            distTo[v] = INFINITY;
        validateVertex(s);
        bfs(G, s, nodoCamino);
    }

    /**
     * Computes the shortest path from any one of the source vertices in {@code sources}
     * to every other vertex in graph {@code G}.
     * @param G the digraph
     * @param sources the source vertices
     * @throws IllegalArgumentException unless each vertex {@code v} in
     *         {@code sources} satisfies {@code 0 <= v < V}
     */
    public BreadthFirstDirectedPaths(WeightedDirectedGraph<K, V> G, Iterable<K> sources, ListaEncadenada<NodoCamino<K>> nodoCamino) {
        marked = new boolean[G.numVertices()];
        distTo = new int[G.numVertices()];
        edgeTo = new int[G.numVertices()];
        for (int v = 0; v < G.numVertices(); v++)
            distTo[v] = INFINITY;
        validateVertices(sources);
        bfs(G, sources, nodoCamino);
    }

    // BFS from single source
    private void bfs(WeightedDirectedGraph<K, V> G, K s, ListaEncadenada<NodoCamino<K>> nodoCamino) {
        QueueUnaLlave<K> q = new QueueUnaLlave<K>();
        marked[(int)s] = true;
        distTo[(int)s] = 0;
        q.enqueue(s);
        NodoCamino<K> nodo = new NodoCamino<>();
        while (!q.isEmpty()) {
            K v = q.dequeue();
            for (K w : G.verticesAdyacentesIT(v)) {
                if (!marked[(int)w]) {
                    edgeTo[(int)w] = (int)v;
                    distTo[(int)w] = distTo[(int)v] + 1;
                    nodo = new NodoCamino<>();
                    nodo.setLongitudCamino(distTo[(int)w]);
                    nodoCamino.agregarElementoFinal(nodo);
                    marked[(int)w] = true;
                    q.enqueue(w);
                }
            }
        }
    }

    // BFS from multiple sources
    private void bfs(WeightedDirectedGraph<K, V> G, Iterable<K> sources, ListaEncadenada<NodoCamino<K>> nodoCamino) {
        QueueUnaLlave<K> q = new QueueUnaLlave<K>();
        NodoCamino<K> nodo = new NodoCamino<>();
        for (K s : sources) {
            marked[(int)s] = true;
            distTo[(int)s] = 0;
            q.enqueue(s);
        }
        while (!q.isEmpty()) {
            K v = q.dequeue();
            for (K w : G.verticesAdyacentesIT(v)) {
                if (!marked[(int)w]) {
                    edgeTo[(int)w] = (int)v;
                    distTo[(int)w] = distTo[(int)v] + 1;
                    nodo = new NodoCamino<>();
                    nodo.setLongitudCamino(distTo[(int)w]);
                    nodoCamino.agregarElementoFinal(nodo);
                    marked[(int)w] = true;
                    q.enqueue(w);
                }
            }
        }
    }

    /**
     * Is there a directed path from the source {@code s} (or sources) to vertex {@code v}?
     * @param v the vertex
     * @return {@code true} if there is a directed path, {@code false} otherwise
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public boolean hasPathTo(K v) {
        validateVertex(v);
        return marked[(int)v];
    }

    /**
     * Returns the number of edges in a shortest path from the source {@code s}
     * (or sources) to vertex {@code v}?
     * @param v the vertex
     * @return the number of edges in a shortest path
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public int distTo(K v) {
        validateVertex(v);
        return distTo[(int)v];
    }

    /**
     * Returns a shortest path from {@code s} (or sources) to {@code v}, or
     * {@code null} if no such path.
     * @param v the vertex
     * @return the sequence of vertices on a shortest path, as an Iterable
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public Iterable<Integer> pathTo(K v) {
        validateVertex(v);

        if (!hasPathTo(v)) return null;
        Stack<Integer> path = new Stack<Integer>();
        int x;
        for (x = (int)v; distTo[x] != 0; x = edgeTo[x])
            path.push(x);
        path.push(x);
        return path;
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(K v) {
        int V = marked.length;
        /*
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
            */
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertices(Iterable<K> vertices) {
        if (vertices == null) {
            throw new IllegalArgumentException("argument is null");
        }
        int V = marked.length;
        for (K v : vertices) {
           // aqu� iba alguna exvcepci�n pero se decidi� eliminarla - Michael Osorio
        }
    }

}
