package model.data.structures;

import java.util.Iterator;
import java.util.Stack;

import API.IWeightedDirectedGraph;
import model.extras.In;
import model.extras.StdOut;
import model.extras.StdRandom;

/**
 *  The {@code EdgeWeightedDigraph} class represents a edge-weighted
 *  digraph of vertices named 0 through <em>V</em> - 1, where each
 *  directed edge is of type {@link DirectedEdge} and has a real-valued weight.
 *  It supports the following two primary operations: add a directed edge
 *  to the digraph and iterate over all of edges incident from a given vertex.
 *  It also provides
 *  methods for returning the number of vertices <em>V</em> and the number
 *  of edges <em>E</em>. Parallel edges and self-loops are permitted.
 *  <p>
 *  This implementation uses an adjacency-lists representation, which 
 *  is a vertex-indexed array of {@link Bag} objects.
 *  All operations take constant time (in the worst case) except
 *  iterating over the edges incident from a given vertex, which takes
 *  time proportional to the number of such edges.
 *  <p>
 *  For additional documentation,
 *  see <a href="http://algs4.cs.princeton.edu/44sp">Section 4.4</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */

//public class EdgeWeightedDigraph<K extends Comparable<K>, V > implements IWeightedDirectedGraph<K, V>{
//    private static final String NEWLINE = System.getProperty("line.separator");
//
//    private final int numberOfV;                // number of vertices in this digraph
//    private int E;                      // number of edges in this digraph
//    private ListaEncadenada<DirectedEdge>[] adj;    // adj[v] = adjacency list for vertex v
//    private int[] indegree;             // indegree[v] = indegree of vertex v
//    
//    public EdgeWeightedDigraph() {
//    	this.numberOfV =0;
//    	this.E =0;
//	}
//    
//    /**
//     * Initializes an empty edge-weighted digraph with {@code V} vertices and 0 edges.
//     *
//     * @param  V the number of vertices
//     * @throws IllegalArgumentException if {@code V < 0}
//     */
//    public EdgeWeightedDigraph(int z) {
//        if (z < 0) throw new IllegalArgumentException("Number of vertices in a Digraph must be nonnegative");
//        this.numberOfV = z;
//        this.E = 0;
//        this.indegree = new int[z];
//        adj =  (ListaEncadenada[]) new ListaEncadenada[numberOfV];
//        
//        for (int v = 0; v < z; v++)
//            adj[v] = new ListaEncadenada<DirectedEdge>();
//    }
//    
//
//    /**
//     * Initializes a new edge-weighted digraph that is a deep copy of {@code G}.
//     *
//     * @param  G the edge-weighted digraph to copy
//     */
//    public EdgeWeightedDigraph(EdgeWeightedDigraph G) {
//        this(G.numVertices());
//        this.E = G.numEdges();
//        for (int v = 0; v < G.numVertices(); v++)
//            this.indegree[v] = G.indegree(v);
//        for (int v = 0; v < G.numVertices(); v++) {
//            // reverse so that adjacency list is in same order as original
//            Stack<DirectedEdge> reverse = new Stack<DirectedEdge>();
//            for (DirectedEdge e : G.adj[v]) {
//                reverse.push(e);
//            }
//            for (DirectedEdge e : reverse) {
//                adj[v].agregarElementoFinal(e);
//            }
//        }
//    }
//
//    /**
//     * Returns the number of vertices in this edge-weighted digraph.
//     *
//     * @return the number of vertices in this edge-weighted digraph
//     */
//    public int numVertices() {
//        return numberOfV;
//    }
//
//    /**
//     * Returns the number of edges in this edge-weighted digraph.
//     *
//     * @return the number of edges in this edge-weighted digraph
//     */
//    public int numEdges() {
//        return E;
//    }
//    
//    
//
//    // throw an IllegalArgumentException unless {@code 0 <= v < V}
//    private void validateVertex(int v) {
//        if (v < 0 || v >= numberOfV)
//            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (numberOfV-1));
//    }
//
//    /**
//     * Adds the directed edge {@code e} to this edge-weighted digraph.
//     *
//     * @param  e the edge
//     * @throws IllegalArgumentException unless endpoints of edge are between {@code 0}
//     *         and {@code V-1}
//     */
//    public void addEdge(DirectedEdge e) {
//        int v = e.from();
//        int w = e.to();
//        validateVertex(v);
//        validateVertex(w);
//        adj[v].agregarElementoFinal(e);
//        indegree[w]++;
//        E++;
//    }
//
//
//    /**
//     * Returns the directed edges incident from vertex {@code v}.
//     *
//     * @param  v the vertex
//     * @return the directed edges incident from vertex {@code v} as an Iterable
//     * @throws IllegalArgumentException unless {@code 0 <= v < V}
//     */
//    public Iterable<DirectedEdge> adj(int v) {
//        validateVertex(v);
//        return (Iterable<DirectedEdge>) adj[v];
//    }
//
//    /**
//     * Returns the number of directed edges incident from vertex {@code v}.
//     * This is known as the <em>outdegree</em> of vertex {@code v}.
//     *
//     * @param  v the vertex
//     * @return the outdegree of vertex {@code v}
//     * @throws IllegalArgumentException unless {@code 0 <= v < V}
//     */
//    public int outdegree(int v) {
//        validateVertex(v);
//        return adj[v].darNumeroElementos();
//    }
//
//    /**
//     * Returns the number of directed edges incident to vertex {@code v}.
//     * This is known as the <em>indegree</em> of vertex {@code v}.
//     *
//     * @param  v the vertex
//     * @return the indegree of vertex {@code v}
//     * @throws IllegalArgumentException unless {@code 0 <= v < V}
//     */
//    public int indegree(int v) {
//        validateVertex(v);
//        return indegree[v];
//    }
//
//    /**
//     * Returns all directed edges in this edge-weighted digraph.
//     * To iterate over the edges in this edge-weighted digraph, use foreach notation:
//     * {@code for (DirectedEdge e : G.edges())}.
//     *
//     * @return all edges in this edge-weighted digraph, as an iterable
//     */
//    public Iterable<DirectedEdge> edges() {
//        ListaEncadenada<DirectedEdge> list = new ListaEncadenada<DirectedEdge>();
//        for (int v = 0; v < numberOfV; v++) {
//            for (DirectedEdge e : adj(v)) {
//                list.agregarElementoFinal(e);
//            }
//        }
//        return list;
//    } 
//
//    /**
//     * Returns a string representation of this edge-weighted digraph.
//     *
//     * @return the number of vertices <em>V</em>, followed by the number of edges <em>E</em>,
//     *         followed by the <em>V</em> adjacency lists of edges
//     */
//    public String toString() {
//        StringBuilder s = new StringBuilder();
//        s.append(numberOfV + " " + E + NEWLINE);
//        for (int v = 0; v < numberOfV; v++) {
//            s.append(v + ": ");
//            for (DirectedEdge e : adj[v]) {
//                s.append(e + "  ");
//            }
//            s.append(NEWLINE);
//        }
//        return s.toString();
//    }
//
//	@Override
//	public V darVertice(K id) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public void agregarVertice(K id, V infoVer) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public int numArcos() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	@Override
//	public double darPesoArco(K idOrigen, K idDestino) {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	@Override
//	public void agregarArco(K idOrigen, K idDestino, double peso) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void agregarArco(K idOrigen, K idDestino, double peso, char ordenLexicografico) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public Iterator<K> darVertices() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public int darGrado(K id) {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	@Override
//	public Iterator<K> darVerticesAdyacentes(K id) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public void desmarcar() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public NodoCamino<K>[] DFS(K idOrigen) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public NodoCamino<K>[] BFS(K idOrigen) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public NodoCamino<K>[] darCaminoDFS(K idOrigen, K idDestino) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public NodoCamino<K>[] darCaminoBFS(K idOrigen, K idDestino) {
//		// TODO Auto-generated method stub
//		return null;
//	}

//}