package model.data.structures;

import javax.swing.text.html.HTMLDocument.Iterator;

public class DirectedDFS<K extends Comparable<K>, V>
{
	private boolean[] marked;
	
	public DirectedDFS(WeightedDirectedGraph<K,V> G, K s, ListaEncadenada<NodoCamino<K>> nodoCamino)
	{
		marked = new boolean[G.numVertices()];
		dfs(G, s, nodoCamino);
	}

	public DirectedDFS(WeightedDirectedGraph<K,V> G, Iterable<K> sources , ListaEncadenada<NodoCamino<K>> nodoCamino)
	{
		marked = new boolean[G.numVertices()];
		for (K s : sources)
			if (!marked[(int)s]) dfs(G, s, nodoCamino);
	}

	private void dfs(WeightedDirectedGraph<K,V> G, K v, ListaEncadenada<NodoCamino<K>> nodoCamino )
	{
		marked[(int)v] = true;
		NodoCamino<K> nodo = new NodoCamino<>();
		nodoCamino.agregarElementoFinal(nodo);
		for (K w : G.verticesAdyacentesIT(v))
			if (!marked[(int)w]) dfs(G, w, nodoCamino);
	}
 
	public boolean marked(int v)
	{ return marked[v]; }
	
}
