package model.data.structures;

import java.util.Iterator;

import API.IWeightedDirectedGraph;
import model.VO.VOArco;

public class WeightedDirectedGraph<K extends Comparable<K>, V > implements IWeightedDirectedGraph<K, V>{

	private final int numberOfV; //N�mero de vertices en ese grafo
	private int E; //Numero de aristas en este grafo
	private ListaLlaveValorSecuencial<K, VOArco>[] adj;  //  arbol con los adyacentes del vertice V
	private ArbolBinarioInfo<K,V> vertices; //Vertices del grafo
	private int[] inDegree;  // inDegree[v] = indegree del vertice V

	public  WeightedDirectedGraph() {
		// TODO constructor
		this.numberOfV =0; 
		this.E = 0;
		adj = (ListaLlaveValorSecuencial[]) new ListaLlaveValorSecuencial[0];
		vertices = new ArbolBinarioInfo<>();
	} 
	
	public WeightedDirectedGraph(int numeroVertices){
		this.numberOfV = numeroVertices;
		this.E = 0;
		adj = (ListaLlaveValorSecuencial[]) new ListaLlaveValorSecuencial[numeroVertices];
		vertices = new ArbolBinarioInfo<>();
	}

	@Override
	public int numVertices() {
		// TODO Retorna el n�mero de vertices
		return numberOfV;
	}

	@Override
	public V darVertice(K id) {
		// TODO Retornar un vertice con el id dado por parametro
		return vertices.get(id);
	}

	@Override
	public void agregarVertice(K id, V infoVer) {
		// TODO Agregar un vertice
		vertices.put(id, infoVer);
	}

	@Override
	public int numArcos() {
		// TODO Auto-generated method stub
		return E;
	}

	@Override
	public double darPesoArco(K idOrigen, K idDestino) {
		// TODO dar el peso de un arco
		return 0;
	}

	@Override
	public void agregarArco(K idOrigen, K idDestino, double peso) {
		// TODO Agrega un arco de un nodo de salida a un nodo de llegada. Este arco lleva peso
		VOArco<K> pesoAux = new VOArco<K>();
		
		pesoAux.setPeso(peso);
		pesoAux.setOrdenLexicografico('z');
		pesoAux.setId(idDestino);
		
		adj[(int)idOrigen].insertar(idDestino, pesoAux);
		
		
		
	}

	@Override
	public void agregarArco(K idOrigen, K idDestino, double peso, char ordenLexicografico) {
		// TODO Agrega un arco de un nodo de salida a un nodo de llegada. Este nodo lleva peso  orden lexicografico
		VOArco<K> pesoAux = new VOArco<K>();
		
		pesoAux.setPeso(peso);
		pesoAux.setOrdenLexicografico(ordenLexicografico);
		pesoAux.setId(idDestino);
		
		adj[(int)idOrigen].insertar(idDestino, pesoAux);
		
		//TODO Falta ordenar la lista de adyacentes para que est� ordenada por el orden lexicografico
		
				VOArco<K>[] arcos = new VOArco[adj[(int)idOrigen].size()];
				
				int i =0;
				for(K llave: adj[(int)idOrigen].keys()){
					
					arcos[i] = adj[(int)idOrigen].darValor(llave);
					i++;
				}
				
				QuickSort<VOArco<K>> quicArco = new QuickSort<>();
				quicArco.sort(arcos);
				
				ListaLlaveValorSecuencial<K, VOArco> listaAux = new ListaLlaveValorSecuencial<>();
				
				for(int j=0; i < arcos.length; j++){
					listaAux.insertar(arcos[j].getId(), arcos[j]);
				}
				
				adj[(int)idOrigen] = listaAux;
	}

	@Override
	public Iterator<K> darVertices() {
		// TODO Da un iterador con todos los vertices del grafo
		return vertices.keys().iterator();
	}

	@Override
	public int darGrado(K id) {
		// TODO Grado de un nodo con id K
		return adj[(int)id].size();
	}

	public Iterable<K> verticesAdyacentesIT( K id )
	{
		return adj[(int)id].keys();
	}
	@Override
	public Iterator<K> darVerticesAdyacentes(K id) {
		// TODO retorna las llaves de los vertices adyacentes a el vertice con el id dado por parametro
		return adj[(int)id].keys().iterator();
	}

	@Override
	public void desmarcar() {
		// TODO Auto-generated method stub

	}

	@Override
	public NodoCamino<K>[] DFS(K idOrigen) 
	{	
		ListaEncadenada<NodoCamino<K>> lista = new ListaEncadenada<>();
		
		DirectedDFS dfs = new DirectedDFS(this, idOrigen, lista);
		
		NodoCamino<K>[] camino =  (NodoCamino[])new NodoCamino[lista.darNumeroElementos()];
		
		lista.cambiarActualPrimero();
		for(int i =0; i < lista.darNumeroElementos(); i++)
		{
			camino[i] = lista.darElementoPosicionActual();
			lista.avanzarSiguientePosicion();
		}
		return camino;
	}

	@Override
	public NodoCamino<K>[] BFS(K idOrigen) {
		// TODO REtorna los vertices accesibles desde el nodo de origen
		return null;
	}

	@Override
	public NodoCamino<K>[] darCaminoDFS(K idOrigen, K idDestino) {
		// TODO retorna un camino a seguir para llegar de un nodo al otro
		return null;
	}

	@Override
	public NodoCamino<K>[] darCaminoBFS(K idOrigen, K idDestino) {
		// TODO retorna el camino para llegar de un nodo a otro
		return null;
	}


}
