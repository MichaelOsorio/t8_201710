package model.data.structures;

public class NodoCamino<T> {

	int idFinal;
	int idAdy;
	double peso;
	int longitudCamino;
	
	public void setIdFinal(int idFinal){
		this.idFinal = idFinal;
	}
	
	public void setIdAdy(int idAdy){
		this.idAdy = idAdy;
	}
	
	public void setPeso(double peso){
		this.peso = peso;
	}
	
	public void setLongitudCamino(int longitudCamino){
		this.longitudCamino = longitudCamino;
	}
	
	public int getIdFinal(){
		return this.idFinal;
	}
	
	public int getIdAdy(){
		return this.idAdy;
	}
	
	public double getPeso(){
		return this.peso;
	}
	
	public int getLongitudCamino(){
		return this.longitudCamino;
	}
	
}
