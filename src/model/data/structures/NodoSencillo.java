package model.data.structures;


public class NodoSencillo<T> {

	private T item;
	
	private NodoSencillo<T> siguiente;
	
	public NodoSencillo(){
		this.item = null;
		this.siguiente = null;
	}
	
	public NodoSencillo(T item, NodoSencillo<T> siguiente){
		//super();
		
		this.item = item;
		this.siguiente = siguiente;
	}
	
	public T darItem(){
		return item;
	}
	
	public void establecerItem(T item){
		this.item = item;
	}
	
	public NodoSencillo<T> darSiguiente(){
		return siguiente;
	}
	
	public void establecerSiguiente(NodoSencillo<T> siguiente){
		this.siguiente = siguiente;
	}
}
