package model.data.structures;

import java.util.Iterator;

public class QueueUnaLlave<T> implements Iterable<T>
{

	private ListaEncadenada<T> list;
	
	public QueueUnaLlave(){
		list = new ListaEncadenada<T>();
	}
	
	public  void enqueue(T item){
		list.enqueue(item);
	}
	
	public T dequeue(){
		return list.dequeue();
	}
	
	public boolean isEmpty(){
		return list.isEmpty();
	}
	
	public int size(){
		return list.darNumeroElementos();
	}

	

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return list.iterator();
	}
	
}
